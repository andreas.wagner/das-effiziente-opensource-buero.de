---
type: post
title: "E-Mail, Kalender, ToDos und Kontaktverwaltung: GNOME Evolution"
---
Das quelloffene Programm »GNOME Evolution« vereint viele Funktionen. Es ist ein
* moderner E-Mail-Client,
* nützlicher Kalender,
* einfacher Notizen-Verwalter
* rudimentärer Kontakt-Manager und
* kleiner ToDo-Listen-Verwalter.

## Ein Blick auf die E-Mail-Verwaltung
Standardmäßig zeigt Evolution auf der linken Seite oben die E-Mail-Konten und
unten den Ansichten-Umschalter. Mit letzterem kann man zwischen den Ansichten
»E-Mail«, »Kontakte«, »Kalender«, »Aufgaben« und »Notizen« wechseln.

Klickt man auf einen E-Mail-Ordner (zum Beispiel »Eingang«), werden die E-Mails
in der mittleren Ansicht gezeigt. Hier auf dem Bild ist der Ordner leer, da ich
E-Mails, um die ich mich nicht mehr kümmern will, oft genug lösche. Sind Mails
in dem Ordner, kann man sie anklicken um sie in die Ansicht in der Mitte unten zu
laden.
![Evolution in der E-Mail-Ordneransicht, Ordner leer](/posts/img/03-002-Evolution_Ordneransicht.png)

## Und wie sieht der Kalender aus?
Hier sieht man den Kalender von Evolution. Man kann Mit einem Rechtsklick auf einen
Tag wird ein Kontextmenü geöffnet, über welches man zum Beispiel einen neunen Termin
eintragen kann. Mein Terminkalender für den Juni 2022 sah zum Beispiel so aus:
![Evolution in der Kalenderansicht](/posts/img/03-001-Evolution_Kalenderansicht.png)
Der Kalender wird über Google-Calendar mit dem Mobiltelefon und anderen Rechnern
synchronisiert. Es werden auch andere Synchronisationsmethoden unterstützt.
