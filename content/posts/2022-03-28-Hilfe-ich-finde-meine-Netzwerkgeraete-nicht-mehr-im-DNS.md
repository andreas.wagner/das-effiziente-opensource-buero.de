---
type: post
title:  "»Hilfe, ich finde meine Netzwerkgeräte im DNS nicht mehr«"
---
Manchmal sind die Netzwerkgeräte im Netz nicht mehr per DNS auffindbar. Das passiert normalerweise,
wenn der Heim-Router (Speedport, FritzBox, …) neu gestartet wird. Das liegt daran, dass viele
Netzwerkgeräte ihren Netzwerknamen dem Router beim Verbinden mitteilen. Beim Neustart vergisst der
Router die mitgeteilten Informationen.<!--more-->

Die Lösung ist für LAN-Geräte recht einfach: Bei dem Gerät den Netzwerkstecker
ziehen, etwas warten und wieder anstöpseln. Dann werden die Netzwerknamen erneut
übertragen.

Wenn man mehrere Access-Points für das WLAN hat, wird der Neustart des Routers
normalerweise auch nicht im Netzwerk mitgeteilt. Die WLAN-Geräte neu zu starten
reicht normalerweise.

# Was ist »DNS«?
»DNS« bedeutet »Domain Name System« und ist eine gängige Methode, um gut
lesbaren Netzwerknamen IP-Adressen zuzuordnen.

DNS-Name     | IP-Adresse
------------ | -------------
tel-andreas  | 192.168.2.105
raspberry2   | 192.168.2.103
speedport.ip | 192.168.2.1

Dabei ist es erlaubt, mehreren DNS-Namen ein und die selbe IP-Adresse zuzuweisen.
