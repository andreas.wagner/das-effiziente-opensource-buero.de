---
type: 'post'
title:  "Anti-Spam-Maßnahmen auf dieser Seite"
---
Diese Seite setzt sehr einfache Anti-Spam-Maßnahmen ein: Wer im
E-Mail-Körper keinen Empfänger-Namen angibt, wird abgewiesen.

<!-- more -->

Wer also Andreas Wagner anschreiben will, muss mindestens "Andreas"
oder "Wagner" in der E-Mail haben.
