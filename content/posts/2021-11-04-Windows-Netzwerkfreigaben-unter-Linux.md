---
type: 'post'
title:  "Samba: Windows-Netzwerkfreigaben unter Linux"
---
Mit »Samba« steht ein leistungsfähiger Dienst zur Verfügung, der Windows-Dateifreigaben
unter Linux ermöglicht. Freigegebene Verzeichnisse werden unter Windows so verfügbar, wie
eine Freigabe von Windows aus.
<!--more-->

# Freigabe ohne Passwort einrichten

Ein Beispiel für eine Konfiguration, bei der alle anonym angemeldeten Benutzer in die Freigabe
schreiben können, sieht so aus:

```
 [public]
 path = /media/pi/Verbatim
 guest ok = yes
 browseable = yes
 read only = no
 force user = pi
 force group = pi
```

Die Freigabe nennt sich »public« und es werden die Daten die unter »/media/pi/Verbatim« abgelegt
sind, freigegeben. Das ist in meinem Fall eine USB-Festplatte, die von dem Benutzer »pi«
eingehangen wurde.

»guest ok = yes« heißt, dass man kein Benutzerkonto auf dem Raspberry braucht, um sich an den
Dateien bedienen zu können. »browseable = yes« sagt dem Samba-Server, dass er die Freigabe
auch nennt, wenn er gefragt wird, welche Freigaben er zur Verfügung hat.

Mit »read only = no«, »force user = pi«, »force group = pi« wird die Beschreibbarkeit für anonyme
Benutzer festgelegt. Alles, was man in die FReigabe schreibt, wird über den Benutzer »pi« mit der
Gruppe »pi« abgewickelt. Für solche Zwecke kann man aber auch einen Gast-Account und eine
Gast-Gruppe erstellen.

# Heimat-Verzeichnisse freigeben

Ein Beispiel aus der Standard-Konfiguration bei einem Debian-Derivat:

```
[homes]
   comment = Home Directories
   browseable = no

# By default, the home directories are exported read-only. Change the
# next parameter to 'no' if you want to be able to write to them.
   read only = yes

# File creation mask is set to 0700 for security reasons. If you want to
# create files with group=rw permissions, set next parameter to 0775.
   create mask = 0700

# Directory creation mask is set to 0700 for security reasons. If you want to
# create dirs. with group=rw permissions, set next parameter to 0775.
   directory mask = 0700
   
# By default, \\server\username shares can be connected to by anyone
# with access to the samba server.
# The following parameter makes sure that only "username" can connect
# to \\server\username
# This might need tweaking when using external authentication schemes
   valid users = %S
```

Hier kommt die Anweisung »comment = ...« neu hinzu. Neben dem Netzwerknamen können
Freigaben auch einn Kommentar enthalten, der näher beschreibt, welchen Zweck diese
Freigabe erfüllt.

»create mask = 0700« und »directory mask = 0700« geben an, mit welchen
Berechtigungen standardmäßig abgespeichert wird. Das erste Zeichen hat eine 
besondere Bedeutung, wenn es ungleich Null ist. Das benötigt man aber in der Regel
nicht. Die Ziffer an zweiter Stelle gibt die Rechte des Benutzers an. Die an
dritter Stelle der Gruppe und die vierte Stelle gibt die Rechte aller anderen
Benutzer an. Wie man die Zahlen dekodiert, gibt die folgende Tabelle an:

```
0 keine Rechte
1 ausführbar
2 beschreibbar
3 ausführbar und beschreibbar
4 lesbar
5 les- und ausführbar
6 les und beschreibbar
7 les-, ausführ- und beschreibbar
```

»valid users = %S« sagt aus, dass der Benutzer wie das Share heißen muss. Genauere
Angaben zu der Konfiguration eins Samba-Servers findet man in der offizellen
Dokumentation unter [https://www.samba.org/samba/docs/].

# Was »Samba« sonst noch kann: Benutzerauthentifizierung unter Windows und Linux

»Samba« hat die Fähigkeit, Benutzerauthentifizierungen für Windows und Linux
durchzuführen. Also so, dass sich Linux- und Windows-Benutzer anmelden
können. Ich habe damit aber noch keine Erfahrung. Man benötigt inzwischen eine
Business-Variante von MS Windows (»Windows 10 Professional« oder teurer), um die
Anmeldung zentral auszulagern.
