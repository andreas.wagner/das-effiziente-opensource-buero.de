# make -j 4     # für 4 parallele Imagemagick-"convert"-Aufrufe

files_in = $(wildcard *.jpg)
files_out = $(foreach data,$(files_in),$(data:jpg=)jpeg)

all: $(files_out)

%.jpeg: %.jpg
	convert $< -flatten -define trim:percent-background=20% -fuzz 2% -background white -trim +repage $@

.PHONY: all
