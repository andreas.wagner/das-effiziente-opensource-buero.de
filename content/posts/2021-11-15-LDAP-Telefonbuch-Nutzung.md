---
type: post
title:  "Gemeinsames Telefonbuch nutzen mit LDAP"
---
Das Lightweight Directory Access Protocol »LDAP« dient vielen Zwecken. Heute möchte ich
seinen Nutzen als gemeinsames Telefonbuch aufzeigen. Wenn man die ersten
Konfigurations-Hürden genommen hat, ist es einfacher zu verwalten als ein Telefonbuch auf dem Telefon.<!--more-->
Man kann alle Daten mit der Tastatur eingeben und muss nicht auf dem Telefon vier Mal die
»3« drücken um das kleine »e« zu bekommen.

# Beispiel-Eintrag

Ein LDAP-Eintrag enthält zusammengehörige Paare von Werten, zum Beispiel kann ein Eintrag
folgerndermaßen aussehen:
![Beispiel für einen LDAP-Telefonbucheintrag](/posts/img/02-004-Beispiel.png)

Beziehungsweise (mit falschen Telefonnummern, damit Telefonwerber schwerer rankommen):
```
cn: Andreas Wagner
sn: Wagner
telephoneNumber: 01234-56789012
Mobile: 0171-00000000 
```
Einem Eintrag können andere Einträge in einer Baumstruktur untergeordnet sein. Wie die
Verzeichnisse/Ordner auf Ihrem Computer. Das ist für eine Nutzung als Telefonbuch aber
meines Wissens irrelevant, deswegen gehe ich darauf nicht näher ein.

# Installation: Erkläre ich ein andermal

Wie man ein OpenLDAP installiert werde ich erklären, nachdem Ubuntu 22.04 LTS als stabil
gekennzeichnet wurde. Bis dahin verweise ich auf die
[LDAP-Installationslanleitung von Ubuntu](https://ubuntu.com/server/docs/service-ldap).

# LDAP-Nutzung am Beispiel eines Grandstream-VoIP-Telefons

Ich möchte noch zeigen, wie meine Konfiguration an meinem Grandstream-VoIP-Telefon
aussieht. Zunächst die Login-Seite:

![Login](/posts/img/02-001-Login.png)
Hier meldet man sich als »admin« an.

![LDAP auswählen](/posts/img/02-002-LDAP-auswaehlen.png)
Man wechselt auf die Seite »Contacts« - »LDAP«.

![Beispiel-Konfiguration](/posts/img/02-003-Beispiel.png)
Dort trägt man die Konfiguration des LDAP-Servers ein. Besonderes Augenmerk ist auf die
Zeilen »LDAP Number Filter« und »LDAP Name Filter« zu legen:
* Der »LDAP Number Filter« ist eine Anweisung, wie das Telefon auf dem LDAP-Server
  heraussuchen soll, welcher Eintrag zu der Nummer gehört. In meinem Beispiel lautet die
  Anweisung »(|(telephoneNumber=%)(Mobile=%))«.
* Dar »LDAP Name Filter« ist dem vorgenannten Filter ähnlich. Wenn man auf dem Tastenfeld
  des Telefons im Bereich »LDAP-Suche« einen Namen eingibt, wird der Eintrag
  herausgesucht, auf den der Filterausdruck passt. In meinem Fall lautet der
  Filterausdruck »(|(cn=%*)(sn=%*))«

# Beschreibung zu »LDAP Number Filter« und »LDAP Name Filter«

Ich veranschauliche die erste oben genannte Anweisung mal:
```
(|(telephoneNumber=%)(Mobile=%))
```
Das Zeichen <code>|</code> steht für »oder«. Es soll entweder
<code>(telephoneNumber=%)</code>, <code>(Mobile=%)</code> oder beides wahr sein, um den LDAP-
Eintrag zu selektieren. Dabei ist <code>%</code> die anrufende Telefonnummer oder
angerufene Telefonnummer oder der eingegebene Namensbestandteil.


# Links

* [OpenLDAP](https://www.openldap.org/) — Serverprogramm, welches LDAP bereitstellt
* [LDAP Admin](https://www.openldap.org/) — Clientprogramm, welches zum Bearbeiten der
  abgelegten Daten geeignet ist.
