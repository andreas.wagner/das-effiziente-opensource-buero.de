---
type: 'post'
title:  "Willkommen auf der Seite!"
---
Heute startet die Seite »das-effiziente-opensource-buero.de«. Es ist geplant,
hier Artikel zu quelloffenen Programmen zu veröffentlichen, die das
Büro-Leben vereinfachen. <!-- more --> Einige Themen, die ich mir vorgenommen habe:

 * Wie verwendet man Absatzvorlagen in LibreOffice? Damit wird das Layout eines Textes einheitlich, was gekonnter aussieht. Außerdem wird damit ein automatisches Inahltsverzeichnis möglich. 
 * Wie gibt man unter Linux ein Netzlaufwerk für Windows frei? (Oder: Wie benutzt man »Samba«?)
 * Was ist »Voice over IP« (VoIP) und was ist daran besser als Festnetz oder ISDN?
 * * Wie benutz man es?
 * * Welche Fallstricke gibt es?
 * * Wie konfiguriert man sein Telefon?
 * Was ist ein LDAP-Telefonbuch und warum sollte man es nutzen, wenn man mehrere VoIP-Telefone hat?
 * * Wie konfiguriert man es?
 * Was kann Scribus für mich tun? Das ist ein Textsatzprogramm ähnlich Adobe InDesign.
 * Ist LaTeX noch aktuell? Das ist eine Textbeschreibungssprache, die häufig für mathematische Bücher verwendet wird.
 * Ist das E-Mail-Programm »Evolution« was für mich?
 * PDF 1.3 wird zum zum archivieren verwendet. Warum eigentlich? Was ist das Problem der Nachfolger?
 * Wie mache ich mir mit dem Mind-Mapping-Programm »FreeMind« den Kopf frei?
 * Was taugt ein Raspberry Pi? Kann ich den im Büro gut brauchen? (Ja, selbst die alten Versionen sind gute, stromsparende Büro-Server.)

Ich überlege auch, fortgeschrittenere Themen zu besprechen:

 * Wie geht man mit »screenie« und »screen« um?
 * * »screen« ist ein tolles Programm, wenn man längere Datentransfers macht. Man kann die Verbindung trennen und wiederaufnehmen; die Programme, die man von »screen« aus starten laufen weiter, wie bei RealVNC-Verbindung des Raspberry Pi.
 * * »screenie« managt die »screen«-Sessions: Man kann mit einer Nummer auswählen, welche »screen«-Verbindung wiederaufgenommen werden soll.
 * Wie macht man ein Backup mit »rsync«?

Ich möchte auch die Autoren vorstellen; zum Startzeitpunkt bin aber nur ich
dabei, Andreas Wagner.

