---
type: post
title:  "Inhaltsverzeichnis durch Nutzung von Absatzvorlagen unter LibreOffice"
---
Absatzvorlagen machen Texte erst richtig professionell: Wenn man einen
langen Text (eine Hausarbeit, eine Zeitschrift oder ein Buch) schreibt,
dann muss man sich
nicht von Anfang an über ein hübsches Layout Gedanken machen. Man setzt
die Absatz-Art und ändert diese zum Schluss, wenn die Texte fertig sind. Dann
hat man am schnellsten einen Überblick, ob einem die Schriftart, -größe
und so weiter gefallen.
<!--more-->

# Die Absatzart auswählen

Diese Bilderstrecke zeigt, wie man mit LibreOffice einem Absatz eine Vorlage
zuweist.

![Eine einfache Überschrift](/posts/img/01-004-einfacher-Text.png)
Geben Sie zuerst einen Text ein, dem Sie eine Formatvorlage zuweisen wollen.
In diesem Fall ist es eine Überschrift. Setzen Sie den Cursor in die Zeile,
in welcher der Absatz sich befindet.

![Wählen Sie eine Vorlage aus](/posts/img/01-005-Vorlagenauswahl.png)
Wählen Sie die Vorlage aus, die Sie verwenden wollen. Wenn Sie die
Eigenschaften der Vorlage später verändern, werden alle Textpassagen
angepasst, die diese Vorlage zugewiesen bekommen haben.

![Begutachten Sie das Ergebnis](/posts/img/01-006-Ergebnis.png)
Das Ergebnis ist in diesem Fall eine Überschrift.

# Ein Inhaltsverzeichnis anlegen

Wenn Sie die Formatvorlagen »Überschrift 1« bis »Überrschrift 4« verwenden,
können Sie ein Inhaltsverzeichnis erstellen lassen. VORSICHT: Wenn sich ein
Eintrag in dem Verzeichnis ändert, muss man es aktualisieren lassen. Dazu
später mehr.

![Wie man ein Inhaltsverzeichnis anlegt](/posts/img/01-001-LibreOffice-Menue-Einfuegen-Verzeichnis.png)
Um ein Inhaltsverzeichnis anzulegen, manövrieren Sie den Cursor an die
Stelle, an der das Verzeichnis erzeugt werden soll. Dann klicken Sie in der
Menüleiste auf »Einfügen« und dann auf »Verzeichnis«. In dem sich öffnenden
Menü klicken Sie auf den gleichnamigen Eintrag; also wieder »Verzeichnis«.

![Dialog zum Inhaltsverzeichnis](/posts/img/01-002-Verzeichnis-Typ-Inhaltsverzeichnis.png)
In dem Dialog, der sich dann auftut, klicken Sie auf »Ok«.

![Ergebnis](/posts/img/01-003-Ergebnis.png)
Anschließend wird das Inhaltsverzeichnis an die Stelle gesetzt, an
welcher der Cursor war.

# Das Inhaltsverzeichnis aktualisieren

Klicken Sie mit der rechten Maustaste auf das Inhaltsverzeichnis und wählen
Sie den Popup-Menüpunkt »Inhaltsverzeichnis aktualisieren«.
