---
type: post
title:  "WLAN erweitern mit Access Points"
---
Wenn man gutes WLAN in allen Räumen haben will, ist es bei größeren Gebäuden meist hilfreich,
mehrere »Access Points« zu betreiben. Access Points sind im Prinzip so etwas wie die WLAN-Router,
die man kennt. Sie verbinden aber nicht mit dem Internet direkt, sondern mit dem Heimnetzwerk.
<!--more-->

# Fallstricke
Auf dem Access Point darf kein DHCP-Server laufen! Sonst gibt es Ärger mit der IP-Adressvergabe
im Heimnetz.

Viele Router bieten diese Konfigurationsmöglichkeit an. Bei manchen ist es dann der "Bridge-Modus".

Um einen access-point-tauglichen Router zu konfigurieren, schließt man ihn am besten nur an einen
einzelnen Rechner an. Mit diesem stellt man die Konfiguration ein, so dass keine Probleme bei der
IP-Adressvergabe entstehen:
 * DHCP-Server ausschalten,
 * IP-Adresse vergeben
   * (manuell oder
   * per DHCP)
 * ggf. Netzwerkname (DNS) vergeben

Zu beachten ist hier, dass der Access Point seine eigene IP-Adresse durchaus von einem fremden
DHCP-Server bekommen darf. Er darf nur selbst keinen DHCP-Server laufen lassen.
